import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable'
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/map';
declare var window: any;

@Injectable()
export class ServiceApiProvider {
  APEX_API = window.APEX_API;
  OUT_API = window.OUT_API;
  corsProxy = window.corsProxy;

  constructor(private http: Http) {}

  getLogin(path) {
    return this.http.get(this.OUT_API + path, this.jwt()).map((response: Response) => response.json());
  }
  getApi(path) {
    return this.http.get(this.OUT_API + path, this.jwt()).map((response: Response) => response.json());
  }
  apexApi(path) {
    return this.http.get(this.APEX_API + path).map((response: Response) => response.json());
  }

  postLogin(path, formData) {
    return this.http.post(this.OUT_API + path, formData).map((response: Response) => response.json());
  }

  postCreateCoupon(path, formData) {
    return this.http.post(this.OUT_API + path, formData, this.jwt()).map((response: Response) => response.json());
  }

  getLocal(file) {
    return this.http.get('assets/' + file).map((response: Response) => response.json());
  }
  getUserApi(path) {
    return this.http.get(this.OUT_API + path, this.jwt()).map((response: Response) => response.json());
  }
  jwt() {
    const currentUserToken = localStorage.getItem('api_token');
    const headers = new Headers({
      "Accept" : 'application/json',
      'Authorization': 'Bearer ' + currentUserToken,
      'Content-Type': 'application/json; charset=UTF-8'
    });
    return new RequestOptions({ headers: headers });
  }
}
