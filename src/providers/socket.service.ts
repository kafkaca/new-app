import { Injectable } from '@angular/core';
import { Socket } from 'ng-socket-io';
import { Observable } from 'rxjs/Observable';
import { WebsocketService } from './web-socket/web-socket';
import { Events } from 'ionic-angular';
import * as state from "../interfaces/state-manage";
import * as _ from 'underscore';

//https://github.com/thelgevold/rxjs-socket.io
//https://forum.ionicframework.com/t/failed-to-navigate-no-provider-for-lookup/86559/7
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

declare var window: any;

@Injectable()
export class SocketService {
  socketStatus: boolean = false
  updated = {}
  constructor(
    private http: Http,
    private socket: Socket,
    public wsService: WebsocketService,
    public events: Events, ) {
      this.wsService.startSocket();
     }

  sendMessage(msg: string) {
    console.log('SOCKET LISTEN ++');
    //  this.socket.emit('type', 'betting');

    //this.socketListen()
  }

  socketListen() {




    /*
    this.getSocket()
      .subscribe(msg => {
        let SD = _.values(msg.data) //SOCKET DATA
        //console.log("1");

        for (let SI of SD) { // SI is Socket Item
          let ci = SI.details ? SI.details.matchid : false

          if (LiveStarted[ci]) {
            //console.log("2");
            // console.log(SI.details);
            LiveSocket[ci] = SI.details
            Object.assign(LiveStarted[ci], SI.details);
            //Object.assign(LiveStarted[ci], SI.details);
            //kupon kontrolü yap
            if (SI.markets && LiveStarted[ci].markets && SI.betstatus !== 'stopped') {
              let MO = LiveStarted[ci].markets
              let MN = SI.markets

              for (var key in MO) {
                // this.liveStarted[ci].markets[key].active = MN[key].active

                  if (MN[key] && MN[key].active === "0") {
                    LiveStarted[ci].markets[key].active = "0"
                  }
                  else if (MO.hasOwnProperty(key) && MN[key]) {
                  if (state.CCT && state.CCT[key]) {
                    console.log("Kupon İçinde Var - ", state.CCT[key]);

                  }
                  // console.log("Market Var 999999999");
                  if (MO[key].odds && MN[key].odds && Array.isArray(MO[key].odds)) {
                    MO[key].odds.map((odd, i) => {
                      if (!_.isUndefined(MN[key].odds[odd.type])) {

                        let NODD = MN[key].odds[odd.type]
                        if (odd.value < NODD.value) {
                          MN[key].odds[odd.type]['action'] = 'oup';
                          LiveUpdated[key + NODD.typeid] = Date.now()
                        }
                        else if (odd.value > NODD.value) {
                          MN[key].odds[odd.type]['action'] = 'odown';
                          LiveUpdated[key + NODD.typeid] = Date.now()
                        }
                      }

                    })
                    MN[key].odds = _.values(MN[key].odds)
                    Object.assign(LiveStarted[ci].markets[key], MN[key])
                    // console.log(this.liveStarted[ci].markets[key]);
                  }
                }
              }
            }
          }
        }
       // console.log(LiveSocket);

        setTimeout(() => {
          if (_.size(LiveUpdated)) {
            for (var index in LiveUpdated) {
              if (LiveUpdated[index] < Date.now() - 2500) {
                delete LiveUpdated[index]
              }
            }
          }
        }, 3000)
      })
*/

    /*
        setInterval(() => {
          if (_.size(LiveUpdated)) {
            for (var index in LiveUpdated) {
              if (LiveUpdated[index] < Date.now() - 2500) {
                delete LiveUpdated[index]
              }
            }
          }
        }, 3000)*/
  }
  getSocket() {
    console.log('SOCKET OPEN');

    return this.socket.fromEvent<any>("live").map(res => {
      return res;
    });

  }

  close() {
    console.log('SOCKET CLOSED');
    // window.ws.close();
    this.socketStatus = false
    this.socket.removeAllListeners()
    this.socket.disconnect()
  }


}

