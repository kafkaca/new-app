import { Injectable } from '@angular/core';
import * as _ from 'underscore';
import { Events } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ServiceApiProvider } from './service-api/service-api';
import * as state from "../interfaces/state-manage";
import { CCT } from "../interfaces/state-manage";

@Injectable()
export class ActionCoupons {
    un: any = _
    currentCoupons: any = {}
    currentCouponType: string = 'combine'
    currentCouponAmount: number = 3
    createdCoupon: any = 0
    constructor(
        public events: Events,
        private serviceApi: ServiceApiProvider,
        public storage: Storage,
        public toastCtrl: ToastController
    ) {
        this.events.subscribe('coupon:oddupdate', () => {
            //  console.log('odd Update');

            this.updateOdd()
        });
    }

    updateCoupon = (couponItem, marketid) => {
        //  console.log(marketid, couponItem);



        if (
            this.currentCoupons[marketid] &&
            this.currentCoupons[marketid]['odd'].typeid == couponItem['odd'].typeid
        ) {
            delete CCT[marketid]
            delete this.currentCoupons[marketid]
        } else {
            CCT[marketid] = couponItem
            this.currentCoupons[marketid] = couponItem
        }
        this.events.publish('coupon:update');
        // console.log(CCT);
        return true;
    }

    removeCoupon = (marketid) => {
        delete CCT[marketid]
        delete this.currentCoupons[marketid]
        this.events.publish('coupon:oddupdate');
        this.events.publish('coupon:update');
        return true;
    }

    createCoupon(): void {
        this.currentCoupons = state.CCT
        /*  let sendCoupon = {
              "type": this.currentCouponType,
              "amount": this.currentCouponAmount,
              "combine": "",
              "bonus": false,
              "bets": []
          }*/
        let sendCoupon = state.CC
        // console.log(this.currentCoupons);

        if (this.un.size(this.currentCoupons) > 0) {
            this.un.values(this.currentCoupons).map((match) => {
                let { marketid, matchid, odd, sport, match_amount } = match

                let betitem = {
                    "market": marketid,
                    "outcome": odd.typeid,
                    "sport": sport, //reference at reference()
                    "sbv": "",
                    "value": odd.value,
                    "bet": odd.typeid,
                    "match_amount": match_amount
                }
                sendCoupon.bets.push(betitem)
            })
            console.log('OLUSTU KUPON', sendCoupon);
        }

        this.createdCoupon = sendCoupon
        this.events.publish('coupon:created');
    }

    createCouponSend(): void {
        this.createCoupon()
        setTimeout((): void => {
            this.serviceApi.postCreateCoupon('my/coupons/updateCouponDetail', this.createdCoupon).subscribe((res) => {
                state.CC.bets = []
                console.log(res);

                this.presentToast(res.reason)
            })
        }, 500);
    }

    cleanCoupon() {
        for (var key in state.CCT) {
            if (state.CCT.hasOwnProperty(key)) {
                delete state.CCT[key];
            }
        }
        this.events.publish('coupon:oddupdate');
        // console.log("temizlendi");

    }

    changeCoupon(type): void {
        // console.log(state.CC);

        state.CC.type = type
        this.currentCouponType = type
    }

    changePriceSingle = (marketid, matchid, value) => {
        CCT[marketid]['match_amount'] = value
        // this.currentCoupons[marketid]['match_amount'] = value
    }
    bakBanaCoupon = (match, marketid, odd, sportType = 1): void => {
        // console.log(match);

        let matchItem = {}
        matchItem['market'] = _.omit(match[marketid], 'odds')
        matchItem['match'] = _.omit(match, 'markets')
        matchItem['odd'] = odd
        matchItem['sport'] = sportType
        matchItem['matchid'] = match.matchid
        matchItem['marketid'] = marketid
        matchItem['match_amount'] = 1

        if (state.CCT[marketid] && state.CCT[marketid].odd.typeid == odd.typeid) {
            delete state.CCT[marketid]

        } else {
            state.CCT[marketid] = matchItem
        }
        this.events.publish('coupon:oddupdate');
        this.updateOdd()
        return;
    }

    updateOdd() {
        let values = _.pluck(state.CCT, 'odd')
        let totalOdd = 0
        let totalOdd1 = 0
        values.map((odd, i) => {
            if (i === 0) {
                totalOdd1 = parseFloat(odd.value)
            } else {
                totalOdd1 *= parseFloat(odd.value)
            }
            // console.log(totalOdd1);

        })
        state.CC.totalodd = totalOdd1
        state.CC.totalMarkets = values.length

        /*  for (var odd of values) {

              totalOdd += parseFloat(odd.value)
          }
          state.CC.totalodd = totalOdd
          */
    }

    reference() {
        /*  switch ($kind) {
              case 0:
              return 'prematch';
              break;

              case 1:
              return 'live';
              break;

              case 2:
              return 'virtual';
              break;

              default:
              return 'live';
              break;
          }*/
    }


    presentToast(msg) {
        const toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'top'
        });

        toast.onDidDismiss(() => {
            console.log('Dismissed toast');
        });

        toast.present();
    }
}
