import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ServiceApiProvider } from './service-api/service-api';
import { UserState } from './../interfaces/user-options';
import * as state from "../interfaces/state-manage";
declare var window: any;
@Injectable()
export class UserData {
  APEX_API = window.APEX_API;
  OUT_API = window.OUT_API;
  corsProxy = window.corsProxy;
  _favorites: string[] = [];
  HAS_LOGGED_IN = 'hasLoggedIn';
  HAS_SEEN_TUTORIAL = 'hasSeenTutorial';

  constructor(
    public events: Events,
    public storage: Storage,
    public serviceApi: ServiceApiProvider
  ) { }

  hasFavorite(sessionName: string): boolean {
    return (this._favorites.indexOf(sessionName) > -1);
  };

  addFavorite(sessionName: string): void {
    this._favorites.push(sessionName);
  };

  removeFavorite(sessionName: string): void {
    let index = this._favorites.indexOf(sessionName);
    if (index > -1) {
      this._favorites.splice(index, 1);
    }
  };

  login(resData: any): void {
    Object.assign(UserState, resData);
    localStorage.setItem('api_token', resData.api_token)
    this.storage.set('UserState', resData);
  };

  signup(username: string): void {
    this.storage.set(this.HAS_LOGGED_IN, true);
    this.setUsername(username);
    this.events.publish('user:signup');
  };

  logout(): void {
    this.storage.remove(this.HAS_LOGGED_IN);
    this.storage.remove('username');
    this.events.publish('user:logout');
    state.UD.hasLoggedin = false
  };

  setUsername(username: string): void {
    this.storage.set('username', username);
  };

  getUserState(): Promise<string> {
    return this.storage.get('UserState').then((value) => {
      return value;
    });
  };

  hasLoggedIn(): Promise<boolean> {
    return this.storage.get(this.HAS_LOGGED_IN).then((value) => {
      state.UD.hasLoggedin = true
      return value === true;
    });
  };

  checkHasSeenTutorial(): Promise<string> {
    return this.storage.get(this.HAS_SEEN_TUTORIAL).then((value) => {
      return value;
    });
  };



}
