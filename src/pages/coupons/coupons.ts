import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ActionCoupons } from "../../providers/actions-coupons";
import * as _ from 'underscore';
import { Events } from 'ionic-angular';
import * as state from "../../interfaces/state-manage";

@Component({
  selector: 'page-coupons',
  templateUrl: 'coupons.html',
})
export class CouponsPage {
  un: any = _
  state: any = state
  currentCoupons: any
  firstBar: string = 'one'
  secondBar: string = 'combine'
  notification: string = ''
  constructor(
    public ACP: ActionCoupons,
    public navCtrl: NavController,
    public navParams: NavParams,
    public events: Events,
  ) {
    console.log(state);

    // 

  }


  ionViewDidLoad() {
    //this.state = state 
    this.currentCoupons = state.CCT
    // this.currentCoupons = this.actionCoupons.currentCoupons
    this.events.subscribe('coupon:update', () => {
      this.currentCoupons = state.CCT
      //  this.currentCoupons = this.actionCoupons.currentCoupons
    });
  }

  subEvent() {
    this.events.subscribe('coupon:add', (match, market, ) => {
    });
  }
  getTotalCombine = () => {
    let values = _.pluck(state.CCT, 'odd')
    let total;
    if (_.size(state.CCT) > 1) {
      let total = _.reduce(values, function (first: any = 0, second: any = 0) {
        return parseFloat(first.value) + parseFloat(second.value);
      })
    } else if (_.size(state.CCT) == 1) {
      total = values[0].value
    } else {
      total = 0
    }

    console.log(_.size(state.CCT));

    return total
  }

  getTotalSingle() {
    let values = _.pluck(state.CCT, 'match_amount')
    let total = _.reduce(values, function (memo: number = 0, num: number = 0) { return Number(memo) + Number(num); }, 0)
    return total
  }

  cleanCoupon() {
    for (var key in state.CCT) {
      if (state.CCT.hasOwnProperty(key)) {
        delete state.CCT[key];
      }
    }
    this.events.publish('coupon:oddupdate');
  }

  updateCoupon() { }

  remove = (marketid) => {
    // delete state.CCT[marketid]
    this.events.publish('coupon:oddupdate');
    this.ACP.removeCoupon(marketid)
  }

  update(eventName: string = 'fake', sendData: any = {}) {
    this.events.publish('coupon:' + eventName, sendData);
  }

  changeType(couponType): void {
    this.ACP.changeCoupon(couponType.value)
  }

  createCoupon(): void {
    this.ACP.createCoupon()
  }
  createCouponSend(): void {
    this.ACP.createCouponSend()
  }
  openModal() { }

  closeModal() {
    this.navCtrl.pop();
  }

  changeMatchPrice(event): void {
    let { market, matchid } = event.target.dataset
    this.ACP.changePriceSingle(market, matchid, event.target.value)
    // console.log(event)
  }
  changeCouponPrice(event): void {
    // let { market, matchid } = event.target.dataset
    if (event.target.value > 0) {
      state.CC.amount = event.target.value
    } else {
      state.CC.amount = 3
    }


    console.log(state.CC.amount);

    this.ACP.currentCouponAmount = event.target.value
    // console.log(event)
  }
}
