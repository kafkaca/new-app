import { EventProvider } from './../../providers/event/event';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, MenuController } from 'ionic-angular';
import { AlertController, NavController } from 'ionic-angular';
import { UserData } from '../../providers/user-data';
import * as state from "../../interfaces/state-manage";
//http://jasonwatmore.com/post/2016/12/01/angular-2-communicating-between-components-with-observable-subject
//https://angular-2-training-book.rangle.io/handout/observables/using_observables.html
@Component({
  selector: 'page-account',
  templateUrl: 'account.html'
})
export class AccountPage {
  userState: any;
  firstPage: string = 'my-coupons'
  rootPage: any = "HomePage";
  NP: any = state.NP;
  constructor(
    public alertCtrl: AlertController,
    public userData: UserData,
    public events: EventProvider
  ) {
    this.userData.getUserState().then(userState => {
      this.userState = userState
    })
  }

  ngAfterViewInit() {
    //this.getUsername();
  }

  updatePicture() {
    console.log('Clicked to update picture');
  }


}
