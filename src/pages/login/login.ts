import { ServiceApiProvider } from './../../providers/service-api/service-api';
import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NavController } from 'ionic-angular';
import { UserData } from '../../providers/user-data';
import { UserOptions } from '../../interfaces/user-options';


@Component({
  selector: 'page-user',
  templateUrl: 'login.html'
})
export class LoginPage {
  login: UserOptions = { login: '', password: '', remember: false };
  submitted = false;

  constructor(public navCtrl: NavController, public userData: UserData, public serviceApi: ServiceApiProvider) {

    localStorage.setItem('locale', 'tr-TR');
  }

  onLogin(form: NgForm) {
    this.submitted = true;

    if (form.valid) {
      this.serviceApi.postLogin('login', this.login).subscribe((res) => {
        this.navCtrl.setRoot('Account', { mypageid: 'home' });
        this.userData.login(res);
      })




      /*this.userData.login(this.login.username);
      this.navCtrl.push('Account');*/
    }
  }

  onSignup() {
    this.navCtrl.push('Signup');
  }
}
