import { Component, Input, OnInit } from '@angular/core';
import { PreCounts } from './../../interfaces/pre-state';
import { GeneralProvider } from './../../providers/general/general';
import { ActionCoupons } from "../../providers/actions-coupons";
import * as state from "../../interfaces/state-manage";
import * as _ from 'underscore';
@Component({
  selector: 'prematch-sports',
  templateUrl: 'prematch-sports.html'
})
export class PrematchSportsComponent {
  PreCounts:any
  @Input() headerColor: string = '#222';
  @Input() textColor: string = '#FFF';
  @Input() contentColor: string = '#fff';
  @Input() title: string = 'demo accordion';
  @Input() hasMargin: boolean = true;
  @Input() itemType: string;
  @Input() categoryid: any;
  expanded: any = {};
  state: any = state
  viewHeight: number = 100;
  un = _
  updated = {}


  constructor(
    public ACP : ActionCoupons,
    public GP: GeneralProvider) {
      this.PreCounts = PreCounts
    }


  toggleAccordion(exp) {
    this.expanded[exp] = !this.expanded[exp]
  }



}

