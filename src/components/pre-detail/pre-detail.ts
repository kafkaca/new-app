import { Component, OnInit, Input } from '@angular/core';
import { PreCurrent } from "../../interfaces/pre-state";
import { ActionCoupons } from './../../providers/actions-coupons';
import * as _ from 'underscore';
import * as state from "../../interfaces/state-manage";
@Component({
  selector: 'pre-detail',
  templateUrl: 'pre-detail.html'
})
export class PreDetailComponent implements OnInit {
  @Input() preid: any
  state: any = state
  PreCurrent: any
  un: any = _
  expanded: any = {};
  constructor(
    public ACP: ActionCoupons,
  ) { }

  ngOnInit() {
    this.PreCurrent = PreCurrent
   }
  toggleAccordion(exp) {
    this.expanded[exp] = !this.expanded[exp]
  }
}
