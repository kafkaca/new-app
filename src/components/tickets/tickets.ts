import { ServiceApiProvider } from './../../providers/service-api/service-api';
import { Component } from '@angular/core';
@Component({
  selector: 'tickets',
  templateUrl: 'tickets.html'
})
export class TicketsComponent {

  tickets: any;

  constructor(
    public serviceApi: ServiceApiProvider
  ) {
    serviceApi.getUserApi('my/tickets').subscribe(resData => {
      this.tickets = resData.tickets
    })
  }



}
