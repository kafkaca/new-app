import { Component } from '@angular/core';
import { MenuController } from 'ionic-angular';
@Component({
  selector: 'root-menu',
  templateUrl: 'root-menu.html'
})
export class RootMenuComponent {

  constructor(public menuCtrl: MenuController) {
    
     }
    
     openMenu() {
       this.menuCtrl.open();
     }
    
     closeMenu() {
       this.menuCtrl.close();
     }
    
     toggleMenu() {
       this.menuCtrl.toggle();
     }
    
    }