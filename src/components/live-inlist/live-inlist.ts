import { Component, Input } from '@angular/core';
import { ActionCoupons } from "../../providers/actions-coupons";
import { GeneralProvider } from './../../providers/general/general';
import * as _ from 'underscore';
import * as state from "../../interfaces/state-manage";

@Component({
  selector: 'live-inlist',
  templateUrl: 'live-inlist.html'
})
export class LiveInlistComponent {
  un = _
  expanded: any = { live: true };
  state: any = state
  limit: any = {live : 5}
  @Input() liveStarted: any;
  @Input() headerColor: string = '#000';
  @Input() textColor: string = '#FFF';
  @Input() contentColor: string = '#fff';
  @Input() title: string = 'demo accordion';
  @Input() hasMargin: boolean = true;
  @Input() itemType: string;
  @Input() updated: any;
  @Input() upcomingsList: any;


  constructor(
    public ACP: ActionCoupons,
    public GP: GeneralProvider) {}

  toggleAccordion(exp) {
    this.limit[exp] = 8
    this.expanded[exp] = !this.expanded[exp]
  }

 limitUp(exp){
    this.limit[exp] = this.limit[exp] + 5
  }



}
