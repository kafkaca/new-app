import { Pipe, PipeTransform } from '@angular/core';
import { Storage } from '@ionic/storage';
import { LangSports } from './../../interfaces/lang-sports';
import { LangCategories } from './../../interfaces/lang-categories';
import { LangTournaments } from './../../interfaces/lang-tournaments';
import { LangMarkets } from './../../interfaces/lang-markets';
import { LangOdds } from './../../interfaces/lang-odds';
import { NP } from "../../interfaces/state-manage";
import * as _ from 'underscore';
/**
 * Generated class for the GetDataPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'getData',
})
export class GetDataPipe implements PipeTransform {

  constructor(
    public storage: Storage
  ) { }
  transform(value: any, ...args) {
    //console.log(args);

    let resValue: any
    if (_.isString(value)) {
      switch (value) {
        case 'sports':
          resValue = _.sortBy(_.values(LangSports), (item: any) => { return Number(item.sort) })
          break;
        case 'upcomings':
          resValue = new Promise<any>((resolve, reject) => {
            resolve({ first: 1 });
          });
          break;
        case 'demo':
          resValue = this.storage.get('preSports').then((sportsValue) => {
            return _.values(sportsValue);
          })
          break;
        case 'preUpcoming':
          resValue = this.storage.get('preUpcoming').then((preUpcoming) => {
            return preUpcoming;
          })
          break;
        case 'preUpcomingTournaments':

          resValue = this.storage.get('preUpcoming').then((preUpcoming) => {
            let getSports = _.groupBy(_.sortBy(preUpcoming, 'sort'), 'sport_id')[NP.data['sportid']]
            let tournaments = _.values(_.groupBy(getSports, 'category_id'))
            return tournaments
          })
          break;
        case 'started':
          let { sportid } = args[1]
          if (sportid) {
            resValue = _.groupBy(args[0], 'sport_id')[sportid]
          } else {
            resValue = args[0];
          }
          break;

        case 'limit:upcoming':

          if (args[1]) {
            resValue = args[0].slice(0, args[1])
          } else {
            resValue = args[0];
          }
          break;
        case 'limit:live':
          if (args[1]) {
            resValue = args[0].slice(0, args[1])
          } else {
            resValue = args[0];
          }
          break;
          case 'live:sports:count':
          resValue = this.storage.get('liveSportCount').then((sportsValue) => {
            return _.keys(sportsValue);
          })
          break;
        case 'upcoming:dates':

          if (args[1] && _.has(args[1], "sportid")) {
            let { sportid } = args[1]
            return this.storage.get('preMatches').then((value) => {
              let allMatches = value
              for (var key in allMatches) {
                if (allMatches.hasOwnProperty(key)) {
                  var d = new Date(allMatches[key].eventdate * 1000);
                  allMatches[key]['eventDay'] = d.getDate();
                }
              }
              allMatches = _.filter(_.values(allMatches), (item, i) => {
                return item.sport_id == sportid
              })
              return _.groupBy(allMatches, 'eventDay')
            });
          } else {
            resValue = value;
          }
          break;
      }
    }
    return resValue;
  }
}
