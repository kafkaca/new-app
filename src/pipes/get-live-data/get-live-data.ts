import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'underscore';
const firstOdds = {
  '1' : ['20','64','50','830'],
  '2' :  ['20','737','8974','50'],
  '5' : ['710','711','894','895' ],
  '23' : ['50','7102','7103','830' ],
  '109' : ['61392','7102','71400','81398' ],
  '29' : ['64','613','50','830' ],
  '19' : ['7102','64','50','830' ],
  '3' : ['6335','6198','737','863' ],
  '4' : ['20','64','50','716']
}
const sbvs = {
  '50' : 1,
  '711' : 1,
  '895' : 1,
  '7103' : 1
}

@Pipe({
  name: 'getLiveData',
})
export class GetLiveDataPipe implements PipeTransform {

  transform(value: any, type: any, sport:number = 0, all: boolean = false) {
    if (_.isObject(value) && _.isString(type)) {
      if (type == 'market:filter') {
        let tempList = {}
        console.log(_.size(value));
        let sortValues = _.sortBy(_.values(value), 'sbv')
        for (let val of sortValues) {


          if (tempList[val.typeid + val.subtype + val.type]) {
            delete value[val.id]
          } else {
            tempList[val.typeid + val.subtype + val.type] = val
          }
        }
        if (!all && sport && _.isArray(firstOdds[sport])) {
          console.log(sport);

       let filterSort = _.values(value).filter((val, key)=>{
            return (firstOdds[sport].includes(val.typeid + val.subtype))
          })
          return _.sortBy(filterSort, 'typeid')
        }
        else {
        console.log('sport yok');
        return _.values(value)
      }
      }
    }

    if (_.isArray(value) && _.isString(type)) {
      if (type == 'market:filter') {
        let tempList = {}
        _.map(_.sortBy(_.values(value), 'sbv'), (val) => {
          if (tempList[val.typeid + val.subtype + val.type]) {
            delete value[val.id]
          } else {
            tempList[val.typeid + val.subtype + val.type] = val
          }
        });
        // console.log(value);
        return _.values(value)
      }
    }
  }
}
