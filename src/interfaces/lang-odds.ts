export const LangOdds: any = {
    "prematch": {
        "1": "1",
        "2": "X",
        "3": "2",
        "4": "Evet",
        "5": "Hayır",
        "6": "Üst",
        "7": "Alt",
        "8": "2",
        "9": "Tek",
        "10": "Çift",
        "11": "1/1",
        "12": "X/1",
        "13": "2/1",
        "14": "1/X",
        "15": "X/X",
        "16": "2/X",
        "17": "1/2",
        "18": "X/2",
        "19": "2/2",
        "20": "1:0",
        "21": "2:0",
        "22": "2:1",
        "23": "3:0",
        "24": "3:1",
        "25": "3:2",
        "26": "4:0",
        "27": "4:1",
        "28": "4:2",
        "29": "5:0",
        "30": "5:1",
        "31": "6:0",
        "32": "6:1",
        "33": "7:0",
        "34": "7:1",
        "35": "8:0",
        "36": "8:1",
        "37": "9:0",
        "38": "10:0",
        "39": "0:0",
        "40": "1:1",
        "41": "2:2",
        "42": "3:3",
        "43": "0:1",
        "44": "0:2",
        "45": "1:2",
        "46": "0:3",
        "47": "1:3",
        "48": "2:3",
        "49": "0:4",
        "50": "1:4",
        "51": "2:4",
        "52": "0:5",
        "53": "1:5",
        "54": "0:6",
        "55": "1:6",
        "56": "0:7",
        "57": "1:7",
        "58": "0:8",
        "59": "1:8",
        "60": "0:9",
        "61": "0:10",
        "62": "3:4",
        "63": "4:3",
        "64": "4:4",
        "65": "2:5",
        "66": "2:6",
        "67": "5:2",
        "68": "6:2",
        "69": "1X",
        "70": "12",
        "71": "X2",
        "72": "0",
        "73": "1",
        "74": "2",
        "75": "3+",
        "76": "3",
        "77": "4",
        "78": "5",
        "79": "6+",
        "81": "0-1 gol",
        "82": "2-3 gol",
        "92": "4-5 gol",
        "93": "1.",
        "94": "2.",
        "95": "Eşit",
        "96": "6:3",
        "97": "6:4",
        "98": "2. set",
        "99": "3. set",
        "100": "4 set",
        "101": "5 set",
        "102": "Alt ve Evsahibi",
        "103": "Alt ve Beraberlik",
        "104": "Alt ve Deplasman",
        "105": "Üst ve Evsahibi",
        "106": "Üst ve Beraberlik",
        "107": "Üst ve Deplasman",
        "108": "İlk skoru evsahibi yapar ve evsahibi kazanır",
        "109": "Ev sahibi ilk skoru yapar ve berabere biter",
        "110": "Ev sahibi ilk skoru yapar ve Deplasman kazanır",
        "111": "Deplasman ilk skoru yapar ve Evsahibi kazanır",
        "112": "Deplasman ilk skoru yapar ve berabere biter",
        "113": "Deplasman ilk skoru yapar ve Deplasman kazanır",
        "114": "Gol yok",
        "115": "6:5",
        "116": "7:5",
        "117": "7:6",
        "118": "3:6",
        "119": "4:6",
        "120": "5:6",
        "121": "5:7",
        "122": "6:7",
        "123": "Evsahibi > 10",
        "124": "Evsahibi 6-10",
        "125": "Evsahibi 1-5",
        "126": "Deplasman 1-5",
        "127": "Deplasman 6-10",
        "128": "Deplasman > 10",
        "129": "1. çeyrek",
        "130": "2. çeyrek",
        "131": "3. çeyrek",
        "132": "4. çeyrek",
        "133": "Eşitler",
        "134": "Tek",
        "135": "1. periyot",
        "136": "2. periyot",
        "137": "3. periyot",
        "138": "Evsahibi > 4",
        "139": "Evsahibi 3-4",
        "140": "Evsahibi 1-2",
        "141": "Deplasman 1-2",
        "142": "Deplasman 3-4",
        "143": "Deplasman > 4",
        "144": "0-1",
        "145": "2",
        "146": "3",
        "147": "4+",
        "148": "0-10",
        "149": "11-25",
        "150": "26-40",
        "151": "41+",
        "152": "0-30",
        "153": "31-45",
        "154": "46-60",
        "155": "61-75",
        "156": "76+",
        "157": "0",
        "158": "1",
        "159": "4",
        "160": "5",
        "161": "6",
        "162": "7",
        "163": "8",
        "164": "9",
        "165": "10",
        "166": "11",
        "167": "12+",
        "168": "0-3",
        "169": "6+",
        "170": "0-0 0-0",
        "171": "0-0 1-0",
        "172": "0-0 0-1",
        "173": "0-0 2-0",
        "174": "0-0 1-1",
        "175": "0-0 0-2",
        "176": "0-0 3-0",
        "177": "0-0 2-1",
        "178": "0-0 1-2",
        "179": "0-0 0-3",
        "180": "0-0 4+",
        "181": "1-0 1-0",
        "182": "1-0 2-0",
        "183": "1-0 1-1",
        "184": "1-0 3-0",
        "185": "1-0 2-1",
        "186": "1-0 1-2",
        "187": "1-0 4+",
        "188": "0-1 0-1",
        "189": "0-1 1-1",
        "190": "0-1 0-2",
        "191": "0-1 2-1",
        "192": "0-1 1-2",
        "193": "0-1 0-3",
        "194": "0-1 4+",
        "195": "2-0 2-0",
        "196": "2-0 3-0",
        "197": "2-0 2-1",
        "198": "2-0 4+",
        "199": "1-1 1-1",
        "200": "1-1 2-1",
        "201": "1-1 1-2",
        "202": "1-1 4+",
        "203": "0-2 0-2",
        "204": "0-2 1-2",
        "205": "0-2 0-3",
        "206": "0-2 4+",
        "207": "3-0 3-0",
        "208": "3-0 4+",
        "209": "2-1 2-1",
        "210": "2-1 4+",
        "211": "1-2 1-2",
        "212": "1-2 4+",
        "213": "0-3 0-3",
        "214": "0-3 4+",
        "215": "4+ 4+",
        "216": "3+",
        "217": "1 Normal Süre",
        "218": "1 Uzatmalar",
        "219": "1 SO",
        "220": "2 Normal Süre",
        "221": "2 Uzatmalar",
        "222": "2 SO",
        "223": "2+",
        "224": "Her iki takım",
        "225": "0-1",
        "226": "2",
        "227": "3",
        "228": "4+",
        "229": "0-2",
        "230": "3-4",
        "231": "5-6",
        "232": "7+",
        "233": "Evsahibi > 2",
        "234": "Evsahibi 2",
        "235": "Evsahibi 1",
        "236": "Deplasman 1",
        "237": "Deplasman 2",
        "238": "Deplasman > 2",
        "239": "0-4",
        "240": "0-8",
        "241": "9-11",
        "242": "12+",
        "244": "1-15",
        "245": "16-30",
        "246": "31-45",
        "247": "46-60",
        "248": "61-75",
        "249": "76-90",
        "250": "1-10",
        "251": "11-20",
        "252": "21-30",
        "253": "31-40",
        "254": "41-50",
        "255": "51-60",
        "256": "61-70",
        "257": "71-80",
        "258": "81-90",
        "259": "diğer",
        "260": "0 set",
        "261": "1 set",
        "262": "101-110",
        "263": "111-120",
        "264": "121-130",
        "265": "131-140",
        "266": "141-150",
        "267": "151-160",
        "268": "161-170",
        "269": "171-180",
        "279": "<100.5",
        "280": ">180.5",
        "291": "Evsahibi > 14",
        "292": "Evsahibi 8-14",
        "293": "Evsahibi 1-7",
        "294": "Deplasman 1-7",
        "295": "Deplasman 8-14",
        "296": "Deplasman > 14",
        "297": "Evsahibi > 12",
        "298": "Evsahibi 7-12",
        "299": "Evsahibi 1-6",
        "300": "Deplasman 1-6",
        "301": "Deplasman 7-12",
        "302": "Deplasman > 12",
        "303": "Evsahibi / Evet",
        "304": "Evsahibi / Hayır",
        "305": "Berabere / Evet",
        "306": "Berabere / Hayır",
        "307": "Deplasman / Evet",
        "308": "Deplasman / Hayır",
        "309": "Evet / Üst",
        "310": "Evet / Alt",
        "311": "Hayır / Üst",
        "312": "Hayır / Alt",
        "313": "5:3",
        "314": "3:5",
        "315": "1. Oyuncu 15",
        "316": "1. Oyuncu 30",
        "317": "1. Oyuncu 40",
        "318": "Break",
        "319": "2. Oyuncu 0",
        "320": "2. Oyuncu 15",
        "321": "2. Oyuncu 30",
        "322": "2. Oyuncu 40",
        "323": "Break",
        "324": "1. Oyuncu 0",
        "325": "1. Oyuncu 15",
        "326": "1. Oyuncu 30",
        "327": "1. Oyuncu 40",
        "328": "Break",
        "339": "1:0",
        "340": "1:0",
        "341": "2:0",
        "342": "3:0",
        "343": "0:1",
        "344": "1:1",
        "345": "2:1",
        "346": "0:2",
        "347": "1:2",
        "348": "0:3",
        "352": "10:1",
        "353": "1:10",
        "354": "10:2",
        "355": "2:10",
        "356": "10:3",
        "357": "3:10",
        "358": "10:4",
        "359": "4:10",
        "360": "10:5",
        "361": "5:10",
        "362": "10:6",
        "363": "6:10",
        "364": "10:7",
        "365": "7:10",
        "366": "10:8",
        "367": "8:10",
        "368": "10:9",
        "369": "9:10",
        "373": "Alt",
        "374": "Üst",
        "378": "Alt",
        "379": "Üst",
        "380": "<5",
        "381": "5-6",
        "382": "7+",
        "383": "0-2",
        "384": "3-4",
        "385": "5-6",
        "386": "7+",
        "387": "0-2",
        "388": "3-4",
        "389": "5-6",
        "390": "7+",
        "391": "Alt",
        "392": "Üst",
        "393": "Alt",
        "394": "Üst",
        "395": "0-1",
        "396": "2",
        "397": "3",
        "398": "4+",
        "399": "0-1",
        "400": "2",
        "401": "3",
        "402": "4+",
        "403": "Alt",
        "404": "Üst",
        "405": "Alt",
        "406": "Üst",
        "407": "Tek",
        "408": "Çift",
        "409": "Tek",
        "410": "Çift",
        "411": "0",
        "412": "1",
        "413": "2",
        "414": "3",
        "415": "4",
        "416": "5",
        "417": "6+",
        "418": "0",
        "419": "1",
        "420": "2",
        "421": "3",
        "422": "4+",
        "423": "Alt",
        "424": "Üst",
        "425": "Alt",
        "426": "Üst",
        "427": "0",
        "428": "1",
        "429": "2",
        "430": "3+",
        "431": "0",
        "432": "1",
        "433": "2",
        "434": "3+",
        "435": "Augusto, Renato",
        "436": "Momcilovic, Marko",
        "437": "Hamroun, Jugurtha",
        "438": "Achim, Vlad",
        "439": "Golubovic, Bojan",
        "440": "Nita, Florin",
        "441": "Popa, Adrian",
        "442": "Malyh, Andrey",
        "443": "Andreev, Dmitri",
        "444": "Afonin, Vadim",
        "445": "Bamba, Yacouba",
        "446": "Machado, Paulo",
        "447": "Sanaya, Anzor",
        "448": "Bueno, Ramon",
        "449": "Gonzalez, Mario",
        "450": "Vasiev, Farkhod",
        "451": "Zanev, Petar",
        "452": "Shynder, Anton",
        "453": "Kamolov, Pavel",
        "454": "Zaytsev, Nikolay",
        "455": "Anene, Chuma",
        "456": "Ogude, Fegor",
        "457": "Pnishi, Alban",
        "458": "Basic, Marko",
        "459": "Jovicic, Branko",
        "460": "Jikia, Giorgi",
        "461": "Selihov, Aleksandr Aleksandrovich",
        "462": "Salugin, Aleksandr",
        "463": "Khomich, Dmitry",
        "464": "Navas, Jesus",
        "465": "Hart, Joe",
        "466": "Balanovich, Sergey",
        "467": "Yok",
        "468": "Schonheim, Fabian",
        "469": "Adrian Lopez",
        "470": "Costa, Diego",
        "471": "Azpilicueta, Cesar",
        "472": "Ivanovic, Branislav",
        "473": "Hazard, Eden",
        "474": "Amuzie, Stanley",
        "475": "Sincere, Muenfuh",
        "476": "Matic, Nemanja",
        "477": "Courtois, Thibaut",
        "478": "Telles, Alex",
        "479": "Fabregas, Cesc",
        "480": "Fabiano",
        "481": "Stoch, Miroslav",
        "482": "Pedro",
        "483": "Vasiev, Farkhod",
        "484": "Collins, James",
        "485": "Subasic, Danijel",
        "486": "Reid, Winston",
        "487": "Carroll, Andy",
        "488": "Nordtveit, Haavard",
        "489": "Caballero, Willy",
        "490": "Perotti, Diego",
        "491": "Jesus, Juan",
        "492": "Florenzi, Alessandro",
        "493": "Salah, Mohamed",
        "494": "Alisson",
        "495": "Gulbrandsen, Fredrik",
        "496": "Fazio, Federico",
        "497": "El Shaarawy, Stephan",
        "498": "Szczesny, Wojciech",
        "499": "Iturbe, Juan",
        "500": "Yok",
        "571": "Alt",
        "573": "Üst",
        "595": "Evet",
        "597": "Hayır",
        "599": "Alt",
        "601": "Üst",
        "603": "Alt",
        "605": "Üst",
        "650": "1.5:0.5",
        "651": "0.5:1.5",
        "652": "0 - 27",
        "653": "28 - 34",
        "654": "35 - 41",
        "655": "42 - 48",
        "656": "49 - 55",
        "657": "56 - 62",
        "658": "63 +",
        "659": "0 - 6",
        "660": "7 - 13",
        "661": "14 - 20",
        "662": "21 - 27",
        "663": "42 +",
        "664": "5+",
        "665": "Hayır/ Hayır",
        "666": "Evet / Hayır",
        "667": "Evet / Evet",
        "668": "Hayır/ Evet",
        "671": "Ev sahibi ve üst",
        "673": "Berabere ve üst",
        "675": "Deplasman ve üst",
        "677": "Ev sahibi ve alt",
        "679": "Berabere ve alt",
        "681": "Deplasman ve alt",
        "689": "1 veya 0 / Alt",
        "690": "1 veya 2 / Alt",
        "691": "0 veya 2 / Alt",
        "692": "1 veya 0 / Üst",
        "693": "1 veya 2 / Üst",
        "694": "0 veya 2 / Üst",
        "695": "1 veya 0 / Evet",
        "696": "1 veya 0 / Hayır",
        "697": "1 veya 2 / Evet",
        "698": "1 veya 2 / Hayır",
        "699": "0 veya 2 / Evet",
        "700": "0 veya 2 / Hayır",
        "2438": "Delkin, Artem",
        "2440": "Zunic, Ivica",
        "2442": "Shogenov, Marat",
        "2444": "Rudenko, Aleksandr",
        "2446": "player home 21",
        "2448": "player home 22",
        "2450": "player home 23",
        "2452": "Budakov, Aleksander",
        "2454": "Khurtsidze, David",
        "2456": "Shavaev, Alikhan",
        "2458": "Kostyukov, Mikhail",
        "2460": "Conde, Sekou",
        "2462": "player away 22",
        "2464": "player away 23",
        "2466": "Aina, Ola",
        "2468": "Loftus-Cheek, Ruben",
        "2470": "player home 19",
        "2472": "player home 20",
        "2474": "player home 21",
        "2476": "player home 22",
        "2478": "player home 23",
        "2480": "Emerson",
        "2482": "Paredes, Leandro",
        "2484": "player away 19",
        "2486": "player away 20",
        "2488": "player away 21",
        "2490": "player away 22",
        "2492": "player away 23",
        "3035": "Alt",
        "3037": "Üst",
        "4470": "Alt",
        "4472": "Üst",
        "4474": "Alt",
        "4476": "Üst",
        "4572": "Tek",
        "4574": "Çift",
        "9911": "Abdullah Avcı",
        "9912": "Şenol Güneş",
        "9913": "Mustafa Denizli",
        "9914": "Yılmaz Vural",
        "9915": "Yabancı Teknik Direktör",
        "991121": "Beşiktaş",
        "991122": "Fenerbahçe",
        "991123": "Galatasaray",
        "991124": "Trabzonspor",
        "991125": "Başakşehir",
        "991126": "Diğer",
        "-1": "Yok",
        "-2": "Diğerleri"
    },
    "live": {
        "1": "1",
        "2": "x",
        "3": "2",
        "7": "1",
        "8": "2",
        "9": "x",
        "11": "Üst",
        "12": "Alt",
        "14": "1",
        "15": "x",
        "16": "2",
        "17": "1",
        "18": "2",
        "19": "2:0",
        "20": "2:1",
        "21": "0:2",
        "22": "1:2",
        "23": "3:0",
        "24": "3:1",
        "25": "3:2",
        "26": "0:3",
        "27": "1:3",
        "28": "2:3",
        "29": "2",
        "30": "3",
        "31": "3",
        "32": "4",
        "33": "5",
        "34": "1X",
        "35": "12",
        "36": "X2",
        "37": "0",
        "38": "1",
        "39": "2",
        "40": "3+",
        "41": "Gol var",
        "42": "Gol yok",
        "43": "Tek",
        "44": "Çift",
        "45": "0:0",
        "46": "1:0",
        "47": "2:0",
        "48": "3:0",
        "53": "0:1",
        "54": "1:1",
        "55": "2:1",
        "56": "3:1",
        "61": "0:2",
        "62": "1:2",
        "63": "2:2",
        "64": "3:2",
        "69": "0:3",
        "70": "1:3",
        "71": "2:3",
        "72": "3:3",
        "109": "0",
        "110": "1",
        "111": "2",
        "112": "3+",
        "113": "Diğer",
        "118": "Üst",
        "119": "Alt",
        "120": "Alt",
        "121": "Üst",
        "122": "Alt",
        "123": "Üst",
        "124": "Alt",
        "125": "Üst",
        "126": "Alt",
        "127": "Üst",
        "128": "Tek",
        "129": "Çift",
        "130": "Tek",
        "131": "Çift",
        "132": "Tek",
        "133": "Çift",
        "134": "Tek",
        "135": "Çift",
        "136": "Tek",
        "137": "Çift",
        "138": "Tek",
        "139": "Çift",
        "140": "Evet",
        "141": "Hayır",
        "159": "Alt",
        "160": "Üst",
        "161": "Alt",
        "162": "Üst",
        "163": "Alt",
        "164": "Üst",
        "171": "Tek",
        "172": "Çift",
        "173": "Tek",
        "174": "Çift",
        "175": "Tek",
        "176": "Çift",
        "177": "Alt",
        "178": "Üst",
        "179": "1. Oyuncu 0",
        "180": "1. Oyuncu 15",
        "181": "1. Oyuncu 30",
        "182": "1. Oyuncu 40",
        "183": "2. Oyuncu 0",
        "184": "2. Oyuncu 15",
        "185": "2. Oyuncu 30",
        "186": "2. Oyuncu 40",
        "187": "1. Oyuncu 0",
        "188": "1. Oyuncu 15",
        "189": "1. Oyuncu 30",
        "190": "1. Oyuncu 40",
        "191": "2. Oyuncu 0",
        "192": "2. Oyuncu 15",
        "193": "2. Oyuncu 30",
        "194": "2. Oyuncu 40",
        "195": "1. Oyuncu 0",
        "196": "1. Oyuncu 15",
        "197": "1. Oyuncu 30",
        "198": "1. Oyuncu 40",
        "199": "2. Oyuncu 0",
        "200": "2. Oyuncu 15",
        "201": "2. Oyuncu 30",
        "202": "2. Oyuncu 40",
        "219": "0",
        "220": "1",
        "221": "2",
        "222": "3",
        "223": "4",
        "224": "5",
        "253": "1:0",
        "254": "1:1",
        "255": "2:1",
        "256": "3:0",
        "257": "0:1",
        "258": "1:1",
        "259": "2:1",
        "260": "0:2",
        "261": "1:2",
        "262": "1:3",
        "314": "1. Oyuncu 0",
        "315": "1. Oyuncu 15",
        "316": "2. Oyuncu 30",
        "317": "1. Oyuncu 40",
        "318": "Break",
        "319": "2. Oyuncu 0",
        "320": "2. Oyuncu 15",
        "321": "2. Oyuncu 30",
        "322": "2. Oyuncu 40",
        "323": "Break",
        "324": "1. Oyuncu 0",
        "325": "1. Oyuncu 15",
        "326": "1. Oyuncu 30",
        "327": "1. Oyuncu 40",
        "328": "Break",
        "339": "0:0",
        "340": "1:0",
        "341": "2:0",
        "342": "3:0",
        "343": "0:1",
        "344": "1:1",
        "345": "2:1",
        "346": "0:2",
        "347": "1:2",
        "348": "0:3",
        "357": "01-15 dk.",
        "358": "16-30 dk.",
        "359": "31-45 dk.",
        "360": "46-60 dk.",
        "361": "61-75 dk.",
        "362": "76-90 dk.",
        "363": "Gol yok",
        "364": "İlk yarı",
        "365": "İkinci yarı",
        "366": "Eşit",
        "373": "Alt",
        "374": "Üst",
        "378": "Alt",
        "379": "Üst",
        "380": "<5",
        "381": "5-6",
        "382": "7+",
        "383": "0-2",
        "384": "3-4",
        "385": "5-6",
        "386": "7+",
        "387": "0-2",
        "388": "3-4",
        "389": "5-6",
        "390": "7+",
        "391": "Alt",
        "392": "Üst",
        "393": "Alt",
        "394": "Üst",
        "395": "0-1",
        "396": "2",
        "397": "3",
        "398": "4+",
        "399": "0-1",
        "400": "2",
        "401": "3",
        "402": "4+",
        "403": "Alt",
        "404": "Üst",
        "405": "Alt",
        "406": "Üst",
        "407": "Tek",
        "408": "Çift",
        "409": "Tek",
        "410": "Çift",
        "411": "0",
        "412": "1",
        "413": "2",
        "414": "3",
        "415": "4",
        "416": "5",
        "417": "6+",
        "418": "0",
        "419": "1",
        "420": "2",
        "421": "3",
        "422": "4+",
        "423": "Alt",
        "424": "Üst",
        "425": "Alt",
        "426": "Üst",
        "427": "0",
        "428": "1",
        "429": "2",
        "430": "3+",
        "431": "0",
        "432": "1",
        "433": "2",
        "434": "3+",
        "447": "Oier",
        "467": "Yok",
        "500": "Yok",
        "571": "Alt",
        "573": "Üst",
        "595": "Evet",
        "597": "Hayır",
        "599": "Alt",
        "601": "Üst",
        "603": "Alt",
        "605": "Üst",
        "671": "Evsahibi ve alt",
        "673": "Berabere ve alt",
        "675": "Deplasman ve alt",
        "677": "Evsahibi ve üst",
        "679": "Berabere ve üst",
        "681": "Deplasman ve üst",
        "2438": "Angban, Victorien",
        "2440": "Rico, Fran",
        "2442": "Evsahibi oyuncusu 19",
        "2444": "Evsahibi oyuncusu 20",
        "2446": "Evsahibi oyuncusu 21",
        "2448": "Evsahibi oyuncusu 22",
        "2450": "Evsahibi oyuncusu 23",
        "2452": "Milanov, Georgi",
        "2454": "Larrea, Pablo",
        "2456": "Deplasman oyuncusu 19",
        "2458": "Deplasman oyuncusu 20",
        "2460": "Deplasman oyuncusu 21",
        "2462": "Deplasman oyuncusu 22",
        "2464": "Deplasman oyuncusu 23",
        "2466": "Hernanes",
        "2468": "Espinal, Marcelo",
        "2470": "Evsahibi oyuncusu 19",
        "2472": "Evsahibi oyuncusu 20",
        "2474": "Evsahibi oyuncusu 21",
        "2476": "Evsahibi oyuncusu 22",
        "2478": "Evsahibi oyuncusu 23",
        "2480": "Ajayi, Oluwafemi",
        "2482": "Udo, Ndifreke",
        "2484": "Deplasman oyuncusu 19",
        "2486": "Deplasman oyuncusu 20",
        "2488": "Deplasman oyuncusu 21",
        "2490": "Deplasman oyuncusu 22",
        "2492": "Deplasman oyuncusu 23",
        "3035": "Alt",
        "3037": "Üst",
        "4470": "Alt",
        "4472": "Üst",
        "4474": "Alt",
        "4476": "Üst",
        "4572": "Tek",
        "4574": "Çift",
        "6092": "Alt",
        "6094": "Üst",
        "6100": "Evet",
        "6102": "Hayır"
    }
}