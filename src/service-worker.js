/**
 * Check out https://googlechromelabs.github.io/sw-toolbox/ for
 * more info on how to use sw-toolbox to custom configure your service worker.
 */


'use strict';
importScripts('./build/sw-toolbox.js');
var cacheName = 'ionic-cache';

var cacheFiles = [
  './build/main.js',
  './build/vendor.js',
  './build/main.css',
  './build/polyfills.js',
  'index.html',
  './assets/theme/logo.png',
  './assets/icon.png',
  'manifest.json'
];
/*
self.addEventListener('fetch', function(event) {
  event.respondWith(
    caches.match(event.request, {
      ignoreSearch: true
    }).then(function(response) {
      return response || fetch(event.request);
    })
  );
});
*/

self.addEventListener('fetch', event => {
  if (event.request.method === 'GET') {
    if (event.request.url.startsWith(self.location.origin)) {
      event.respondWith(fetch(event.request));
    }
  }
});
self.addEventListener('install', function (e) {
  console.log('[Service Worker] Install');
  e.waitUntil(
    caches.open(cacheName).then(function (cache) {
      console.log('[Service Worker] Caching app shell');
      return cache.addAll(cacheFiles);
    }).then(function (e) {
      return self.skipWaiting();
    })
  );
});




self.toolbox.options.cache = {
  name: cacheName
};
// pre-cache our key assets
self.toolbox.precache(cacheFiles);
// dynamically cache any other local assets
self.toolbox.router.any('/*', self.toolbox.fastest);
// for any other requests go to the network, cache,
// and then only use that cached resource if your user goes offline
self.toolbox.router.default = self.toolbox.networkFirst;

